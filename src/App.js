import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Home from "./containers/Home/Home";
import AboutUs from "./components/AboutUs/AboutUs";
import Contacts from "./components/Contacts/Contacts";
import Prices from "./components/Prices/Prices";

function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/home" exact component={Home} />
        <Route path="/prices" component={Prices} />
        <Route path="/about" component={AboutUs} />
        <Route path="/contacts" component={Contacts} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
