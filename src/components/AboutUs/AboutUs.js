import React from 'react';
import './AboutUs.css';
import Navigation from "../Navigation/Navigation";

const AboutUs = props => {

    const homeHandler = () => {
        props.history.push({
            pathname: '/home'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const pricesHandler = () => {
        props.history.push({
            pathname: '/prices'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Main-block">
            <div className="Main-nav">
                <Navigation
                    home={homeHandler}
                    about={aboutHandler}
                    prices={pricesHandler}
                    contacts={contactsHandler}
                />
            </div>

            <div className="About-block">
                <h1>About us:</h1>

                <img src="https://ichef.bbci.co.uk/news/800/cpsprodpb/F34D/production/_107058226_.jpg" />

                <p>Member of the American psychological association
                    Member of the Association of Practical Psychologists GIPNART

                    Certified Specialist for Work with Psychosomatic Disorders in Adults
                    Certified Panic Attack Specialist

                    Total work experience - 23 years (I'm 43 now)

                    I work in the areas of deep and short-term psychotherapy.
                    I conduct the reception in person and on-line. You can ask questions for free via Whatsapp, Viber, Telegram</p>

            </div>
        </div>
    );
};

export default AboutUs;