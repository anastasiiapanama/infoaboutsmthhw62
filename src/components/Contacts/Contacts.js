import React from 'react';
import './Contacts.css';
import Navigation from "../Navigation/Navigation";

const Contacts = props => {

    const homeHandler = () => {
        props.history.push({
            pathname: '/home'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const pricesHandler = () => {
        props.history.push({
            pathname: '/prices'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Main-block">
            <div className="Main-nav">
                <Navigation
                    home={homeHandler}
                    about={aboutHandler}
                    prices={pricesHandler}
                    contacts={contactsHandler}
                />
            </div>

            <div className="Contacts-block">
                <h1>How to contact us if you are a customer:</h1>
                <p className="Subtitle-text">Technical Support: If you have difficulty logging into your Teachable account, purchasing products on the website, or any other technical issue, please contact curators@example.com</p>
                <br/>
                <p className="Subtext">Payment support: If you have any difficulties with payments, returns, surcharges, please contact sales@example.com</p>
            </div>
        </div>
    );
};

export default Contacts;