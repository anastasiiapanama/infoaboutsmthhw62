import React from 'react';
import './Navigation.css';

const Navigation = props => {
    return (
        <ul className="nav nav-pills">
            <li className="nav-item">
                <a onClick={props.home} className="nav-link" aria-current="page" href="#">Home</a>
            </li>
            <li className="nav-item">
                <a onClick={props.about} className="nav-link" href="#">About us</a>
            </li>
            <li className="nav-item">
                <a onClick={props.prices} className="nav-link" href="#">Prices</a>
            </li>
            <li className="nav-item">
                <a onClick={props.contacts} className="nav-link" href="#">Contacts</a>
            </li>
        </ul>
    );
};

export default Navigation;