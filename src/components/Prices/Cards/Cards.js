import React from 'react';
import './Cards.css';

const Cards = () => {
    return (
        <div className="Cards">
            <div className="Card">
                <h3>One-time consultation:</h3>
                <p>20 $ per hour</p>
                <p className="Card-text>">Consultation is paid based on the actual duration</p>
            </div>
            <div className="Card">
                <h3>First consultation:</h3>
                <p>15 $ per hour</p>
                <p className="Card-text>">Consultation is paid based on the actual duration</p>
            </div>
            <div className="Card">
                <h3>Group consultation:</h3>
                <p>13 $ per hour</p>
                <p className="Card-text>">Consultation is paid based on the actual duration</p>
            </div>
            <div className="Card">
                <h3>Consultation package:</h3>
                <p>25 $ per hour</p>
                <p className="Card-text>">Consultation is paid based on the actual duration</p>
            </div>
        </div>
    );
};

export default Cards;