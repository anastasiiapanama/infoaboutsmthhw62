import React from 'react';
import Navigation from "../Navigation/Navigation";
import Cards from "./Cards/Cards";
import './Prices.css';

const Prices = props => {

    const homeHandler = () => {
        props.history.push({
            pathname: '/home'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const pricesHandler = () => {
        props.history.push({
            pathname: '/prices'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Main-block">
            <div className="Main-nav">
                <Navigation
                    home={homeHandler}
                    about={aboutHandler}
                    prices={pricesHandler}
                    contacts={contactsHandler}
                />
            </div>
            <div className="Prices-block">
                <h1 className="Prices-title">Consultation prices:</h1>

                <Cards/>
            </div>
        </div>
    );
};

export default Prices;