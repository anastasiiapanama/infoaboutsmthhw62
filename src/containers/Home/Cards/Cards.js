import React from 'react';

const Cards = () => {
    return (
        <>
            <div className="card">
                <div className="face face1">
                    <div className="content">
                        <h2>
                            Depression And Sense Loss
                        </h2>
                        <p>
                            Lack of motivation for activity, apathy and bad mood, a sense of "dead end" in life
                        </p>
                    </div>
                </div>
                <div className="face face2">
                    <h2>01</h2>
                </div>
            </div>
            <div className="card">
                <div className="face face1">
                    <div className="content">
                        <h2>
                            Difficulties At Work
                        </h2>
                        <p>
                            A "dead end" in a career, constant stress at work, lack of understanding with colleagues, a desire to change careers
                        </p>
                    </div>
                </div>
                <div className="face face2">
                    <h2>02</h2>
                </div>
            </div>
            <div className="card">
                <div className="face face1">
                    <div className="content">
                        <h2>
                            Communication Difficulties
                        </h2>
                        <p>Lack of self-confidence, feeling "not the same as everyone else", difficulties in finding a common language</p>
                    </div>
                </div>
                <div className="face face2">
                    <h2>03</h2>
                </div>
            </div>
            <div className="card">
                <div className="face face1">
                    <div className="content">
                        <h2>
                            Eating Disorders
                        </h2>
                        <p>We deal with the reasons, and at the same time with complexes about the figure (if any)</p>
                    </div>
                </div>
                <div className="face face2">
                    <h2>04</h2>
                </div>
            </div>
            <div className="card">
                <div className="face face1">
                    <div className="content">
                        <h2>
                            Crisis Support
                        </h2>
                        <p>Dealing with PTSD, eliminating fears and phobias associated with past events</p>
                    </div>
                </div>
                <div className="face face2">
                    <h2>05</h2>
                </div>
            </div>
        </>
    );
};

export default Cards;