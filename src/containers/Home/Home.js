import React from 'react';
import './Home.css';
import Navigation from "../../components/Navigation/Navigation";
import Cards from "./Cards/Cards";

const Home = props => {

    const homeHandler = () => {
        props.history.push({
            pathname: '/home'
        });
    };

    const aboutHandler = () => {
        props.history.push({
            pathname: '/about'
        });
    };

    const pricesHandler = () => {
        props.history.push({
            pathname: '/prices'
        });
    };

    const contactsHandler = () => {
        props.history.push({
            pathname: '/contacts'
        });
    };

    return (
        <div className="Main-block">
            <div className="Main-nav">
                <Navigation
                    home={homeHandler}
                    about={aboutHandler}
                    prices={pricesHandler}
                    contacts={contactsHandler}
                />
            </div>

            <div className="Container">
                <h1 className="Container-title">Consulting areas:</h1>

                <Cards/>
            </div>
        </div>
    );
};

export default Home;